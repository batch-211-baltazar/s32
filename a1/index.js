let http = require("http");

let welcome = "Welcome to Booking System";
let profile = "Welcome to your profile!";
let courses = "Here's our courses available";
let addcourse = "Add a course to our resources";
let update = "Update a course to our resources";
let archive = "Archive courses to our resources";

let port = 4000

let server = http.createServer(function (request, response) {

	if (request.url == "/" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(welcome);
		response.end()

}

	if (request.url == "/profile" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(welcome);
		response.end()

}

	if (request.url == "/courses" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(courses);
		response.end()

}

	if (request.url == "/addCourse" && request.method == "POST") {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(addcourse);
		response.end()
}

	if (request.url == "/updateCourse" && request.method == "PUT") {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(update);
		response.end()
}

	if (request.url == "/archiveCourse" && request.method == "DELETE") {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(archive);
		response.end()
}

	});

server.listen(port);

console.log('Server running at localhost:4000');